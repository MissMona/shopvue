import { createRouter, createWebHistory } from 'vue-router'
import appIndex from './components/pages/index/index-main.vue'
import aboutUs from './components/pages/About/About-us.vue'


const routes = [
    { path: '/', Component: appIndex },
    { path: '/AboutUs', component: aboutUs },

]

const router = createRouter({
    history: createWebHistory(),
    routes,
})

export default router;